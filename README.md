# Agent Analyser APP

Spark distributed application. Reads Agents CSV dataset and creates new data for further analysis. Data can be exported to CSV or parquet in Hadoop. Creating a table and using Notebook for visual graphs.

It treats original data as an immutable dataset and appends columns to it before exploding to 15 years simulation results.

Analysts can perform queries or create PowerBI reports form it.


## App config

    Config is parsed from application.conf using pureConfig v 0.10.0

        app-name = "agent-analyser-app"
        csv-location = "test-location"
        simulate-years = 15
        affinity-random-factor = 3
    
    
## App command line arguments
    
    Arguments are parsed using scopt v 3.5.0

        brandFactor Double ranging from (0.1 -> 2.9)
    
    
## Main class
    AgentAnalyserApp
    
    
## Running on your local

    AgentAnalyserAppSpec runs the app on sample csv from test resources
    it simulates agent brand changes over next 15 years
    
## Running in production
    run: sbt package
    It will generate jar file in target/scala-2.11/agent-analysis_2.11-0.1.jar
    You can add it to your CI pipeline using Jenkins or Bamboo and publish jar to artifactory
    
    Run it on Hadoop with spark version 2.3.2 using spark-submit or any custom bash script you have for it
    Don't forget to change csv-location in application.conf
    csv-location is in application config as its unlikely to change
    
    I used databricks community version cloud to test it
    

## Testing

    run: sbt test
    Used sparkTesttingBase v 2.3.1 and scalatest v 3.0.5
    Units tests on config parsing, csv reading with correct schema, analysis/simulation
    

    
**Tests also generate random data for property testing.
We can add more of these if we know more properties of the data we are using and how simulations affect them**
    
    
