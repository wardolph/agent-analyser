package io.platform.spark.agent

import org.apache.spark.sql.{Dataset, SparkSession}
import scopt.OptionParser

/**
  * App starting point
  */
class AgentAnalyserApp {

  def main(args: Array[String]): Unit = {

    import pureconfig.generic.auto._
    val config = pureconfig.loadConfigOrThrow[AgentAnalyserConfig]("agent.analyser.config")

    createParser(config.appName).parse(args, CommandLineArguments(1.2)).foreach { arguments =>
      run(config, arguments)
    }

  }

  private[this] def run(config: AgentAnalyserConfig, arguments: CommandLineArguments): Unit ={

    implicit val spark = SparkSession
      .builder()
      .appName(config.appName)
      .getOrCreate()

    import spark.implicits._

    try {
      val agentDataset: Dataset[Agent] = AgentDatasetReader.readCSV(config.csvLocation)

      val result = AgentAnalyser.analyse(agentDataset, arguments.brandFactor, config.simulateYears, config.affinityRandomFactor)
      result.show()

    } finally {
      spark.stop()
    }

  }

  private[this] def createParser(appName: String): OptionParser[CommandLineArguments] = {
    val parser = new scopt.OptionParser[CommandLineArguments](appName) {
      head(appName, "1.0")

      opt[Double]('b', "brandFactor") action { (value, opts) =>
        opts.copy(brandFactor = value)
      } required () text "brandFactor is a Double ranging from (0.1 -> 2.9)"
    }
    parser
  }
}
