package io.platform.spark.agent

import org.apache.spark.sql.Dataset

import scala.annotation.tailrec


object AgentAnalyser {

  /**
    * Entry point for analysis.
    * @param agents
    * @param brandFactor
    * @param simulateYears
    * @param affinityRandomFactor
    * @return
    */
  def analyse(agents: Dataset[Agent], brandFactor: Double, simulateYears: Int, affinityRandomFactor: Int): Dataset[AgentAnalysed] = {
    import agents.sparkSession.implicits._

    val agentAnalysedYearZero: Dataset[AgentAnalysed]= agents.map(AgentAnalysed.apply)

    agentAnalysedYearZero.flatMap(agent => runForYears(agent, brandFactor, simulateYears, affinityRandomFactor))


  }

  /**
    * Used to run simulation for a number of years.
    * Analyse one agent at a time. At first glance it might look like its an application weakness and might explode with stack overflow. It runs only for 15 years and will run on ove specific node.
    * The reason for keeping it like this is the overhead for starting tasks in Hadoop. Task scheduling time should never be more than task running time.
    * @param agent
    * @param brandFactor
    * @param years
    * @param affinityRandomFactor
    * @return
    */
  def runForYears(agent: AgentAnalysed, brandFactor: Double, years: Int, affinityRandomFactor: Int) : Seq[AgentAnalysed] = {


    def analyseAgentForYears(agents: List[AgentAnalysed], simulationYear: Int, wasBreedCAndNC: Boolean) : List[AgentAnalysed] = {
      if(simulationYear == years)
        agents
      else
        agents match {
          case Nil =>
            val analysed = analyseAgent(agent, brandFactor, simulationYear + 1, false, affinityRandomFactor)
            val wasBreedCAndNCReevaluated = agent.New_Breed == "Breed_C" && analysed.New_Breed == "Breed_NC"
            analyseAgentForYears(List(analysed, agent), simulationYear + 1, wasBreedCAndNCReevaluated)
          case x:: tail =>
            val analysed = analyseAgent(x, brandFactor, simulationYear + 1, wasBreedCAndNC, affinityRandomFactor)
            val wasBreedCReevaluated = wasBreedCAndNC || (x.New_Breed == "Breed_C" && analysed.New_Breed == "Breed_NC")
            analyseAgentForYears(List(analysed, x) ::: tail, simulationYear + 1, wasBreedCReevaluated)
        }
    }

    analyseAgentForYears(Nil, 0, false)

  }

  /**
    * Analyse one agent for next year.
    * @param agent
    * @param brandFactor
    * @param simulationYear
    * @param wasBreedCAndNC
    * @param affinityRandomFactor
    * @return
    */
  def analyseAgent(agent: AgentAnalysed, brandFactor: Double, simulationYear: Int, wasBreedCAndNC: Boolean, affinityRandomFactor: Int) : AgentAnalysed = {

    if(agent.Auto_Renew){
      AgentAnalysed(agent, simulationYear, agent.Agent_Breed, false, false, agent.Breed_C_Regained)
    }
    else {
      val rand: Double = Math.random() * affinityRandomFactor
      val affinity = agent.Payment_at_Purchase/ agent.Attribute_Price +
        (rand * agent.Attribute_Promotions * agent.Inertia_for_Switch)


      if(agent.New_Breed == "Breed_C" && affinity < (agent.Social_Grade * agent.Attribute_Brand))
          AgentAnalysed(agent, simulationYear, "Breed_NC", true, false, agent.Breed_C_Regained)

      else if (agent.New_Breed == "Breed_NC" && affinity < (agent.Social_Grade * agent.Attribute_Brand * brandFactor))
        AgentAnalysed(agent, simulationYear, "Breed_C", false, true, wasBreedCAndNC || agent.Breed_C_Regained)
      else
        AgentAnalysed(agent, simulationYear, agent.New_Breed, false, false, agent.Breed_C_Regained)

    }

  }




}
