package io.platform.spark.agent

trait BaseAgent{
  def Agent_Breed: String
  def Policy_ID: Double
  def Age: Int
  def Social_Grade: Int
  def Payment_at_Purchase: Int
  def Attribute_Brand: Double
  def Attribute_Price: Double
  def Attribute_Promotions: Double
  def Auto_Renew: Boolean
  def Inertia_for_Switch: Int
}