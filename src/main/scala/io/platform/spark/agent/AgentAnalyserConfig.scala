package io.platform.spark.agent

final case class AgentAnalyserConfig(appName: String, csvLocation: String, simulateYears: Int, affinityRandomFactor: Int)
