package io.platform.spark.agent

case class Agent(Agent_Breed: String,Policy_ID: Double,Age: Int,Social_Grade: Int,
                 Payment_at_Purchase: Int,Attribute_Brand: Double,Attribute_Price: Double,
                 Attribute_Promotions: Double,Auto_Renew: Boolean,Inertia_for_Switch: Int) extends BaseAgent

