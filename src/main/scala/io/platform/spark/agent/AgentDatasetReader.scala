package io.platform.spark.agent

import org.apache.spark.sql.SparkSession

object AgentDatasetReader {

  def readCSV(CSVLocation: String)(implicit spark: SparkSession) = {
    val df = spark
      .read
      .option("inferSchema", true)
      .option("header", true)
      .csv(CSVLocation)

    import spark.implicits._
    df.as[Agent]
  }

}
