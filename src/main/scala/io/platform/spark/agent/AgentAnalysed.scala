package io.platform.spark.agent


case class AgentAnalysed(Agent_Breed: String,Policy_ID: Double,
                         Age: Int,Social_Grade: Int,Payment_at_Purchase: Int,
                         Attribute_Brand: Double,Attribute_Price: Double,
                         Attribute_Promotions: Double,Auto_Renew: Boolean,
                         Inertia_for_Switch: Int, Simulated_Year: Int,
                         New_Breed: String, Breed_C_Lost: Boolean,
                         Breed_C_Gained: Boolean, Breed_C_Regained: Boolean) extends BaseAgent

object AgentAnalysed{
  def apply(agent: AgentAnalysed, simulatedYear: Int, newBreed: String, breedCLost: Boolean, breedCGained: Boolean, breedCRegained: Boolean) =
    agent.copy(Simulated_Year = simulatedYear, New_Breed = newBreed, Breed_C_Lost = breedCLost, Breed_C_Gained = breedCGained, Breed_C_Regained = breedCRegained)

  /**
    * Converting agent to agent analysed without any changes.
    * Simulated year is 0
    * New brand is same as current
    * @param agent
    * @return
    */
  def apply(agent: Agent) = new AgentAnalysed(agent.Agent_Breed, agent.Policy_ID, agent.Age,
    agent.Social_Grade, agent.Payment_at_Purchase, agent.Attribute_Brand,
    agent.Attribute_Price, agent.Attribute_Promotions, agent.Auto_Renew,
    agent.Inertia_for_Switch, 0, agent.Agent_Breed, false, false, false)

}