package io.platform.spark.agent

import com.holdenkarau.spark.testing.SharedSparkContext
import org.apache.spark.sql.SparkSession
import org.scalatest.prop.Checkers
import org.scalatest.{FlatSpec, Matchers}

class AgentDatasetReaderSpec extends FlatSpec with Matchers with SharedSparkContext with Checkers{

  it should "read csv file and convert to agent dataset" in {


    val path = getClass.getResource("/SimudynePlatformTestData.csv").getPath

    val spark = SparkSession.builder().getOrCreate()

    import spark.implicits._
    val dataset = AgentDatasetReader.readCSV(path)(spark)

    assert(8837 == dataset.count())

  }
}
