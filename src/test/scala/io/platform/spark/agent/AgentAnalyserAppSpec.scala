package io.platform.spark.agent

import com.holdenkarau.spark.testing.{DatasetSuiteBase, SharedSparkContext}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.prop.Checkers

class AgentAnalyserAppSpec extends FlatSpec with Matchers with SharedSparkContext with Checkers{

  it should "read csv from test resources and run analysis" in {

    val csvLocation = getClass.getResource("/SimudynePlatformTestData.csv").getPath

    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val agentDataset: Dataset[Agent] = AgentDatasetReader.readCSV(csvLocation)(spark)

    val result = AgentAnalyser.analyse(agentDataset, brandFactor = 1.2, simulateYears = 15, affinityRandomFactor = 3)

    result.show()
  }

}
