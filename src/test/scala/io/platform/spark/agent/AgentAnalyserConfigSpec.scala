package io.platform.spark.agent

import org.scalatest.{FlatSpec, Matchers}

class AgentAnalyserConfigSpec extends FlatSpec with Matchers {
  it should "parse config correctly" in {

    import pureconfig.generic.auto._

    val config = pureconfig.loadConfigOrThrow[AgentAnalyserConfig]("agent.analyser.config")
    val expectedConfig = AgentAnalyserConfig("agent-analyser-app", "test-location", 15, 3)

    config == expectedConfig

  }
}
