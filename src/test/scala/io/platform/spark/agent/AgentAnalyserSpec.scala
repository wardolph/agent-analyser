package io.platform.spark.agent

import com.holdenkarau.spark.testing.{DatasetGenerator, DatasetSuiteBase, SharedSparkContext}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.scalacheck.{Gen, Prop}
import org.scalatest.prop.Checkers
import org.scalatest.{FlatSpec, Matchers}

object AgentAnalyserSpec{
  val breeds = Seq("Breed_C", "Breed_NC")
}

class AgentAnalyserSpec extends FlatSpec with Matchers with SharedSparkContext with Checkers with DatasetSuiteBase{

  import AgentAnalyserSpec._

  "agent analyser" should "run analysis for 15 years" in {


    val spark = SparkSession.builder().getOrCreate()

    import spark.implicits._

    val agentGenerator = DatasetGenerator.genDataset(spark.sqlContext){

      for {
        agent_Breed <- Gen.oneOf(breeds)
        policy_ID <- Gen.chooseNum[Double](100000001.0, 200000000.0)
        age <- Gen.chooseNum[Int](18, 100)
        social_Grade <- Gen.chooseNum[Int](1,10)
        payment_at_Purchase <- Gen.chooseNum[Int](200,3000)
        attribute_Brand <- Gen.chooseNum[Double](10.0, 100.0)
        attribute_Price <- Gen.chooseNum[Double](5.0, 100.0)
        attribute_Promotions <- Gen.chooseNum[Double](0.1, 15.0)
        auto_Renew <- Gen.oneOf(Seq(true,false))
        inertia_for_Switch <- Gen.chooseNum[Int](0, 10)
      } yield Agent(agent_Breed, policy_ID, age, social_Grade, payment_at_Purchase, attribute_Brand, attribute_Price, attribute_Promotions, auto_Renew, inertia_for_Switch)

    }

    val property = Prop.forAll(agentGenerator) { agents =>

      val analysedAgents = AgentAnalyser.analyse(agents, 1.2, 15, 3)

      agents.count() * 16 == analysedAgents.count()

    }

    check(property)

  }

  "agent analyser" should "produce correct analysis output for 15 years while keeping current year data" in {
    val spark = SparkSession.builder().getOrCreate()

    import spark.implicits._

    val agents = Seq(Agent("Breed_C", 132802001.0 , 66, 3, 250, 25.3, 16.6, 5.1,	false, 9))
    val agentDS = agents.toDS()

    val analysedAgents = AgentAnalyser.analyse(agentDS, 1.2, 15, 3)
    assert(16, analysedAgents.count())

  }

  "agent analyser" should "produce correct analysis output for next year keeping breed NC" in {

    val agent = AgentAnalysed(Agent("Breed_NC", 132802001.0 , 66, 3, 250, 25.3, 16.6, 5.1,	false, 9))


    val expectedAgentAnalysis = AgentAnalysed("Breed_NC",1.32802001E8,66,3,250,25.3,16.6,5.1,false,9,1,"Breed_C",false,true,true)
    val analysedAgent = AgentAnalyser.analyseAgent(agent, 1.2, 1, true, 0)

    assert(expectedAgentAnalysis, analysedAgent)

  }

  "agent analyser" should "produce correct analysis output for next year changing to breed NC" in {

    val agent = AgentAnalysed(Agent("Breed_C", 132802001.0 , 66, 3, 250, 25.3, 16.6, 5.1,	false, 9))
    val expectedAgentAnalysis =     AgentAnalysed("Breed_C",1.32802001E8,66,3,250,25.3,16.6,5.1,false,9,1,"Breed_NC",true,false,false)

    val analysedAgent = AgentAnalyser.analyseAgent(agent, 1.2, 1, false, 0)

    assert(expectedAgentAnalysis, analysedAgent)

  }


}
