import Dependencies.{GeneralLibs, SparkLibs, TestLibs}

name := "agent-analysis"

version := "0.1"

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(SparkLibs.All,
  TestLibs.All, GeneralLibs.All).flatten



lazy val fatJarSettings = Seq(
  artifact in (Compile, assembly) := {
    val art = (artifact in (Compile, assembly)).value
    art.withClassifier(Some("assembly"))
  }
) ++ addArtifact(artifact in (Compile, assembly), assembly)

enablePlugins(BuildInfoPlugin)

mainClass in assembly := Some("io.platform.spark.agent.AgentAnalyserApp")
buildInfoPackage := "io.platform.spark.agent"
