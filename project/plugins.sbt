resolvers += "Artifactory" at "https://artifactory.server.traveljigsaw.com/artifactory/repo"

addSbtPlugin("com.eed3si9n"             % "sbt-assembly"    % "0.14.7")
addSbtPlugin("com.eed3si9n"             % "sbt-buildinfo"   % "0.7.0")