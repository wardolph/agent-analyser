import sbt._
import sbt.Keys._

object Dependencies {

  object SparkLibs{
    val core = "org.apache.spark" %% "spark-core" % "2.3.2"
    val sql = "org.apache.spark" %% "spark-sql" % "2.3.2"
    val hive = "org.apache.spark" %% "spark-hive" % "2.3.2"
    val mllib = "org.apache.spark" %% "spark-mllib" % "2.3.2"


    lazy val All = Seq(core, sql, hive, mllib)
  }

  object TestLibs {
    val sparkTesttingBase = "com.holdenkarau" %% "spark-testing-base" % "2.3.1_0.10.0" % Test
    val scalatest = "org.scalatest" %% "scalatest" % "3.0.5" % Test

    lazy val All = Seq(scalatest, sparkTesttingBase)
  }

  object GeneralLibs {

    val pureConfig = "com.github.pureconfig" %% "pureconfig" % "0.10.0"
    val scopt = "com.github.scopt" %% "scopt" % "3.5.0"
    lazy val All = Seq(pureConfig, scopt)
  }

}
